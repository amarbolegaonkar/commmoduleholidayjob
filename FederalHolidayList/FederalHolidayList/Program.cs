using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Azure;
using SendGrid.Helpers.Mail;
using SendGrid;
using System.Net.Mail;
using System.Net.Mime;
using System.Globalization;
using log4net;

namespace FederalHolidayList
{
  class Program
  {
    private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    static DateTime currentDate = DateTime.UtcNow.Date;
    static string strCurrentDate = String.Format(CultureInfo.InvariantCulture, "{0:yyyy-M-d}", currentDate);
    static string dbCon;
    static SqlCommand cmd;
    static DataTable dt = new DataTable();
    static SqlDataAdapter adp;
    static string cmdStr = string.Empty;
    static string path = System.Environment.CurrentDirectory;
    static string lblWebJobEnvironment = "WebJobEnvironment";
    static string webJobEnvironment = ConfigurationManager.AppSettings["WebJobEnvironment"];
    static void Main(string[] args)
    {
      try
      {
        Console.Out.WriteLine("WebJob Started");
        dbCon = ConfigurationManager.ConnectionStrings["DbCon"].ConnectionString;
        SqlConnection con = new SqlConnection(dbCon);
        DateTime targetDate = new DateTime(currentDate.Year, 12, 25);//new DateTime(currentDate.Year, 12, 25);

        if (targetDate == currentDate)
        {
          SetFederalHolidayList(con);
        }

        GetScheduledHolidayList(con);
      }
      catch(Exception ex)
      {
        Console.Out.WriteLine(ex.Message);
        logger.Error(ex.Message);
        logger.Error(ex.StackTrace);
        logger.Error(ex.InnerException);
        
      }
     
    }
   
    static void GetScheduledHolidayList(SqlConnection con)
    {
      cmdStr = "Select * from PortalCommModule_Holiday where DeliveryDate='" + strCurrentDate + "' and IsObserved=1 and TemplateId!=-1";
      dt = ExecuteSqlCommand(cmdStr, con);

      List<PortalCommModuleHoliday> holidayList = new List<PortalCommModuleHoliday>();
      holidayList = ConvertDataTableToList<PortalCommModuleHoliday>(dt);

      SendHolidayEmail(holidayList, con);
    }

    static void SendHolidayEmail(List<PortalCommModuleHoliday> holidayList, SqlConnection con)
    {
      try
      {
        string emailBodyContent = string.Empty;
        PortalCommModuleTemplates templateDetails;
        List<ClientIdEmailModel> clientEmailList = new List<ClientIdEmailModel>();
        List<CommModuleContactAdvisor> contactList = new List<CommModuleContactAdvisor>();
        string apikey = Convert.ToString(ConfigurationManager.AppSettings["ApiKeySendgrid"]);
        var client = new SendGridClient(apikey);

        cmdStr = "select CompanyEmailUrl from PortalDefaults_EmailSettings";
        dt = ExecuteSqlCommand(cmdStr, con);
        string emailFromUrl = dt.Rows[0][0].ToString();

        cmdStr = "select CompanyName from PortalDefaults_LoginScreen";
        dt = ExecuteSqlCommand(cmdStr, con);
        string companyName = dt.Rows[0][0].ToString();

        SqlCommand contactCmd = new SqlCommand("CommModuleContactAdvisorList", con);
        contactCmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter contactAdp = new SqlDataAdapter(contactCmd);
        DataTable contactDt = new DataTable();
        contactAdp.Fill(contactDt);

        contactList = ConvertDataTableToList<CommModuleContactAdvisor>(contactDt);

        cmdStr = "select * from PortalCommModule_PredefinedKey";
        DataTable preDefinedKeysDt = new DataTable();
        preDefinedKeysDt = ExecuteSqlCommand(cmdStr, con);

        foreach (PortalCommModuleHoliday holiday in holidayList)
        {
          templateDetails = new PortalCommModuleTemplates();
          List<CommModuleContactAdvisor> filterContactList = new List<CommModuleContactAdvisor>();
          var from = new EmailAddress(emailFromUrl, companyName);
          var tos = new List<EmailAddress>();
          var substitutions = new List<Dictionary<string, string>>();

          var subjects = new List<string>();
          DateTime holidayEndDate = new DateTime();
          holidayEndDate = holiday.HolidayDays <= 1 ? holiday.HolidayStartDate : holiday.HolidayStartDate.AddDays(holiday.HolidayDays - 1);

          cmdStr = "select * from PortalCommModule_Templates where Id=" + holiday.TemplateId;
          dt = ExecuteSqlCommand(cmdStr, con);
          List<PortalCommModuleTemplates> templateList = ConvertDataTableToList<PortalCommModuleTemplates>(dt);
          if (templateList.Count > 0)
          {
            templateDetails = templateList[0];
            string templateSubject = templateDetails.TemplateSubject;
            if (holiday.SendToContacts)
            {
              filterContactList.AddRange(contactList.Where(x => x.Type == "Contact").ToList());
            }

            if (holiday.SendToAdvisors)
            {
              filterContactList.AddRange(contactList.Where(x => x.Type == "Advisor").ToList());
            }

            var htmlContent = DownloadAttachmentFromBlob(path, templateDetails.HtmlPath, false);
            List<string> distinctEmails = filterContactList.Select(x => x.Email).Distinct().ToList();

            //Add Entry in PortalCommModule_ScheduleEvent Table
            PortalCommModule_ScheduledEvents scheduledEvent = CreateScheduleEvent(holiday.TemplateId, filterContactList, currentDate, htmlContent, filterContactList.Count);

            while (filterContactList.Count > 0)
            {
              clientEmailList = new List<ClientIdEmailModel>();
              tos = new List<EmailAddress>();
              List<CommModuleContactAdvisor> contactListToSend = new List<CommModuleContactAdvisor>();
              contactListToSend = filterContactList.Take(1000).ToList();
             
              

              foreach (CommModuleContactAdvisor contact in contactListToSend)
              {
                ClientIdEmailModel clientIdEmailItem = new ClientIdEmailModel();
                emailBodyContent = htmlContent;
                if (!string.IsNullOrEmpty(contact.Email))
                {
                  Dictionary<string, string> dynamicField = new Dictionary<string, string>();
                  tos.Add(new EmailAddress(contact.Email));
                  List<CommModule_Predefined> predefinedKey = ConvertDataTableToList<CommModule_Predefined>(preDefinedKeysDt);
                  foreach (CommModule_Predefined key in predefinedKey)
                  {
                    var findstr = htmlContent.Contains(key.PredefinedValue);
                    var findRawstr = false;
                    var rawKeyLabel = string.Empty;
                    var KeyValue = key.PredefinedValue;
                    var hasEnclosed = key.PredefinedValue.Trim().StartsWith("[") && key.PredefinedValue.Trim().EndsWith("]");
                    if (hasEnclosed)
                    {
                        rawKeyLabel = key.PredefinedValue.Substring(1);
                        rawKeyLabel = rawKeyLabel.Remove(rawKeyLabel.Length - 1);
                        findRawstr = htmlContent.Contains(rawKeyLabel);
                    }
                    if (!findstr && findRawstr)
                    {
                        KeyValue = rawKeyLabel;
                    }
                    if (findstr == true || findRawstr == true)
                    {
                      switch (key.PredefinedKey)
                      {

                        case "[UserName]":
                          dynamicField.Add(KeyValue, contact.label);
                          emailBodyContent = emailBodyContent.Replace(KeyValue, contact.label);
                          break;
                        case "[HolidayStartDate]":
                          dynamicField.Add(KeyValue, holiday.HolidayStartDate.ToString("dddd, MMMM dd, yyyy"));
                          emailBodyContent = emailBodyContent.Replace(KeyValue, holiday.HolidayStartDate.ToString("dddd, MMMM dd, yyyy"));
                          break;
                        case "[HolidayName]":
                          dynamicField.Add(KeyValue, holiday.HolidayName);
                          emailBodyContent = emailBodyContent.Replace(KeyValue, holiday.HolidayName);
                          break;
                        case "[HolidayEndDate]":
                          dynamicField.Add(KeyValue, holidayEndDate.ToString("dddd, MMMM dd, yyyy"));
                          emailBodyContent = emailBodyContent.Replace(KeyValue, holidayEndDate.ToString("dddd, MMMM dd, yyyy"));
                          break;
                      }
                    }
                  }
                  //Create List for Adding entry in Email Record Table
                  clientIdEmailItem.ClientId = contact.ClientID;
                  clientIdEmailItem.ContactAdvisorId = contact.id;
                  clientIdEmailItem.emailContent = emailBodyContent;
                  clientIdEmailItem.Email = contact.Email;
                  clientIdEmailItem.Type = contact.Type;

                  substitutions.Add(dynamicField);
                  subjects.Add(templateDetails.TemplateSubject);
                  clientEmailList.Add(clientIdEmailItem);
                }
              }

              var plainTextContent = "Hello -name-";

              var msg = MailHelper.CreateMultipleEmailsToMultipleRecipients(from,
                                                                            tos,
                                                                            subjects,
                                                                            plainTextContent,
                                                                            htmlContent,
                                                                            substitutions
                                                                            );
              msg.AddHeader("Priority", "Urgent");
              msg.AddHeader("Importance", "high");

              //To Get the status From Sendgrid, adding custom args
              foreach (var item in msg.Personalizations)
              {
                item.CustomArgs = new Dictionary<string, string>();
                item.CustomArgs.Add(lblWebJobEnvironment, webJobEnvironment);
                item.CustomArgs.Add("scheduleId", scheduledEvent.ScheduleId.ToString());
              }

              //Removing Empty mail from ToList, before send mail
              string mailAddress = "";
              foreach (var mail in msg.Personalizations.ToList())
              {
                mailAddress = mail.Tos.Select(x => x.Email).First();
                if (string.IsNullOrEmpty(mailAddress))
                {
                  msg.Personalizations.Remove(mail);
                }
              }

              var resp = client.SendEmailAsync(msg).Wait(60000);

              if (resp)
              {
                //Add Entry in Email Record table
                if(scheduledEvent != null)
                {
                  AddEmailRecord(clientEmailList, scheduledEvent, con, emailFromUrl, companyName, templateSubject);                  
                }
                List<string> listToRemove = new List<string>();
                listToRemove = contactListToSend.Select(x => x.Email).ToList();
                filterContactList = filterContactList.Where(x => !listToRemove.Contains(x.Email)).ToList();

              }
            }


            /* Update Next HlidayDate and DeliveryDate For Non-Federal Holidays */
            if (!holiday.IsFederalHoliday)
            {
              string deliveryDate = String.Format(CultureInfo.InvariantCulture, "{0:yyyy-M-d}", holiday.DeliveryDate.AddYears(1));
              string holidayDate = String.Format(CultureInfo.InvariantCulture, "{0:yyyy-M-d}", holiday.HolidayDate.AddYears(1));
              cmdStr = "Update PortalCommModule_Holiday set DeliveryDate='" + deliveryDate + "', HolidayDate='" + holidayDate + "' where Id=" + holiday.Id;
              ExecuteSqlCommandUpdate(cmdStr, con);
            }
          }
        }
      }
      catch(Exception ex)
      {
        Console.Out.WriteLine(ex.Message);
        logger.Error(ex.Message);
        logger.Error(ex.StackTrace);
        logger.Error(ex.InnerException);
       
      }
    }

    static void SetFederalHolidayList(SqlConnection con)
    {
      try
      {
      
      int year = currentDate.Year + 1;// currentDate.Year + 1;

      Dictionary<string, DateTime> holidays = new Dictionary<string, DateTime>();
      PortalCommModuleHoliday holidaysobj = new PortalCommModuleHoliday();
      List<PortalCommModuleHoliday> holidayList = new List<PortalCommModuleHoliday>();
      //year = 2020;
      DayOfWeek dayOfWeek;
      DateTime holidayDt;

      cmdStr = "Select * from PortalCommModule_Holiday where IsFederalHoliday=1 and IsObserved=1";
      dt = ExecuteSqlCommand(cmdStr, con);
      holidayList = ConvertDataTableToList<PortalCommModuleHoliday>(dt);

      foreach (PortalCommModuleHoliday holiday in holidayList)
      {
        holidayDt = new DateTime();
        switch (holiday.Id)
        {
          case 1:
            //NEW YEARS 
            holidayDt = AdjustForWeekendHoliday(new DateTime(year, 1, 1).Date);
            break;
          case 2://Birthday of Martin Luther King, Jr.
            holidayDt = new DateTime(year, 1, 21);
            dayOfWeek = holidayDt.DayOfWeek;
            while (dayOfWeek != DayOfWeek.Monday)
            {
              holidayDt = holidayDt.AddDays(-1);
              dayOfWeek = holidayDt.DayOfWeek;
            }
            break;
          case 3://Washington’s Birthday
            holidayDt = new DateTime(year, 2, 21);
            dayOfWeek = holidayDt.DayOfWeek;
            while (dayOfWeek != DayOfWeek.Monday)
            {
              holidayDt = holidayDt.AddDays(-1);
              dayOfWeek = holidayDt.DayOfWeek;
            }
            break;
          case 4://Memorial Day
            holidayDt = new DateTime(year, 5, 31);
            dayOfWeek = holidayDt.DayOfWeek;
            while (dayOfWeek != DayOfWeek.Monday)
            {

              holidayDt = holidayDt.AddDays(-1);
              dayOfWeek = holidayDt.DayOfWeek;
            }
            break;
          case 5://Independence Day
            holidayDt = AdjustForWeekendHoliday(new DateTime(year, 7, 4).Date);
            break;
          case 6://Labor Day
            holidayDt = new DateTime(year, 9, 1);
            dayOfWeek = holidayDt.DayOfWeek;
            while (dayOfWeek != DayOfWeek.Monday)
            {
              holidayDt = holidayDt.AddDays(1);
              dayOfWeek = holidayDt.DayOfWeek;
            }
            break;
          case 7://Columbus Day
            holidayDt = new DateTime(year, 10, 14);
            dayOfWeek = holidayDt.DayOfWeek;
            while (dayOfWeek != DayOfWeek.Monday)
            {
              holidayDt = holidayDt.AddDays(-1);
              dayOfWeek = holidayDt.DayOfWeek;
            }
            break;
          case 8://Veterans Day
            holidayDt = AdjustForWeekendHoliday(new DateTime(year, 11, 11).Date);
            break;
          case 9://Thanksgiving Day
            var thanksgiving = (from day in Enumerable.Range(1, 30)
                                where new DateTime(year, 11, day).DayOfWeek == DayOfWeek.Thursday
                                select day).ElementAt(3);
            holidayDt = new DateTime(year, 11, thanksgiving);
            break;
          case 10://Christmas Day
            holidayDt = AdjustForWeekendHoliday(new DateTime(year, 12, 25).Date);
            break;
        }


        string deliveryDate = String.Format(CultureInfo.InvariantCulture, "{0:yyyy-M-d}", holidayDt.AddDays(-holiday.DaysBefore));
        string holidayDate = String.Format(CultureInfo.InvariantCulture, "{0:yyyy-M-d}", holidayDt.Date);

        cmdStr = "Update PortalCommModule_Holiday Set HolidayDate='" + holidayDate + "',HolidayStartDate='" + holidayDate + "',DeliveryDate='" + deliveryDate + "' where Id=" + holiday.Id;
        ExecuteSqlCommandUpdate(cmdStr, con);
      }
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine(ex.Message);
        logger.Error(ex.Message);
        logger.Error(ex.StackTrace);
        logger.Error(ex.InnerException);
      }
    }

    //Adjust For WeekendHoliday
    static DateTime AdjustForWeekendHoliday(DateTime holiday)
    {
         return holiday;
    }

    static void ExecuteSqlCommandUpdate(string cmdString, SqlConnection con)
    {
      try
      {
        cmd = new SqlCommand(cmdString, con);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
      }
      catch(Exception ex)
      {
        Console.Out.WriteLine(ex.Message);
        con.Close();
      }
      finally
      {
        con.Close();
      }
    }

    

    public static DataTable ExecuteSqlCommand(string cmdString, SqlConnection con)
    {
      dt = new DataTable();
      cmd = new SqlCommand(cmdString, con);
      adp = new SqlDataAdapter(cmd);
      adp.Fill(dt);

      return dt;
    }

    /// <summary>
    /// Generic Method To "Convert DataTable into List"
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static List<T> ConvertDataTableToList<T>(DataTable dt)
    {
      List<T> data = new List<T>();
      foreach (DataRow dr in dt.Rows)
      {
        T item = GetItem<T>(dr);
        data.Add(item);
      }
      return data;
    }

    /// <summary>
    /// Internal function for 'ConvertDataTableToList' method
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="dr"></param>
    /// <returns></returns>
    private static T GetItem<T>(DataRow dr)
    {
      Type temp = typeof(T);
      T obj = Activator.CreateInstance<T>();

      foreach (DataColumn col in dr.Table.Columns)
      {
        foreach (PropertyInfo pro in temp.GetProperties())
        {
          if (pro.Name == col.ColumnName)
          {
            if (dr[col.ColumnName] != DBNull.Value)
              pro.SetValue(obj, dr[col.ColumnName], null);
          }
          else
          {
            continue;
          }
        }
      }
      return obj;
    }

    /// <summary>
    /// Download Attachment From Blob
    /// </summary>
    /// <param name="sitePath"></param>
    /// <param name="filePath"></param>
    /// <param name="isSend"></param>
    /// <returns></returns>
    public static string DownloadAttachmentFromBlob(string sitePath, string filePath, bool isSend)
    {
      string path = "";
      try
      {
       
      string localPath = string.Empty;
      string fileHtmlText = string.Empty;
      PortalDefaultsBlobDetails blobDetails = new PortalDefaultsBlobDetails();
      cmdStr = string.Empty;
      SqlConnection con = new SqlConnection(dbCon);
      List<PortalDefaultsBlobDetails> blobDetailsList = new List<PortalDefaultsBlobDetails>();

      cmdStr = "Select * from PortalDefaults_BlobDetails";
      dt = ExecuteSqlCommand(cmdStr, con);
      blobDetailsList = ConvertDataTableToList<PortalDefaultsBlobDetails>(dt);
      blobDetails = blobDetailsList[0];

      //blobDetails = entity.PortalDefaults_BlobDetails.FirstOrDefault();
      string subContainerName = blobDetails.BlobContainerName;// Convert.ToString(ConfigurationManager.AppSettings["BlobContainerStore"]);
      string connectionKey = blobDetails.BlobConnectionKey;

      string blobName = "";
      string[] stringSeparators = new string[] { subContainerName };

      string downloadPath = sitePath.Trim('\\') + @"\" + "Downloads" + "\\" + subContainerName;

      
      blobName = filePath.Replace(@"\", @"/");
      blobName = blobName.Substring(0, 1) == "/" ? blobName.Remove(0, 1) : blobName;

      //string connectionKey = WebConfigurationManager.AppSettings["BlobConnectionKey"];

      CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionKey));
      CloudBlobClient storageClient = storageAccount.CreateCloudBlobClient();
      CloudBlobContainer storageContainer = storageClient.GetContainerReference((subContainerName));

      CloudBlockBlob blobSource = storageContainer.GetBlockBlobReference(blobName);

      if (blobSource.Exists())
      {
        //blob storage uses forward slashes, windows uses backward slashes; do a replace
        //  so localPath will be right
        localPath = Path.Combine(@downloadPath, blobSource.Name.Replace(@"/", @"\").Replace(":", ""));
        //if the directory path matching the "folders" in the blob name don't exist, create them
        string dirPath = Path.GetDirectoryName(localPath);
        if (!Directory.Exists(localPath))
        {
          Directory.CreateDirectory(dirPath);
        }
        blobSource.DownloadToFile(localPath, FileMode.Create);
      }
     
      if (Directory.Exists(downloadPath))
      {
        path = localPath;
        fileHtmlText = File.ReadAllText(path);
      }
      if (isSend == true)
      {
        return path;
      }
      else
      {
        return fileHtmlText;
      }
      }
      catch (Exception ex)
      {
        Console.Out.WriteLine(ex.Message);
        logger.Error(ex.Message);
        logger.Error(ex.StackTrace);
        logger.Error(ex.InnerException);
        return path;
      }
    }
   
 
    public static PortalCommModule_ScheduledEvents CreateScheduleEvent(int templateId, List<CommModuleContactAdvisor> contactListToSend, DateTime? onceDeliveryDate, string htmlContent,int contactCount)
    {
     
      SqlConnection con = new SqlConnection(dbCon);
      PortalCommModule_ScheduledEvents scheduleItem = new PortalCommModule_ScheduledEvents();

      try
      {

        string clientIds = string.Empty;
        string emailIds = string.Empty;
        string contactIds = string.Empty;
        foreach (var item in contactListToSend)
        {
          ClientIdEmailModel clientDetail = new ClientIdEmailModel();
          if (!string.IsNullOrEmpty(item.Email))
          {
            clientIds += item.ClientID + ";";
            emailIds += item.id + "~" + item.Email + ";";
            contactIds += item.Type + "~" + item.label + "~" + item.id + ";";

          }

        }
        int NoticeType = 19;
        int deliveryType = 1;
        int scheduleType = 5;
        string ScheduleWeekDays = null;
        string attachments = "";
        string insertQuery = "(" + templateId + ",'" + htmlContent.Replace("'", "''") + "'," + NoticeType + "," + scheduleType + "," + deliveryType + ",'" + clientIds + "','" + contactIds.Replace("'", "''") + "','" + emailIds + "',0,'" + ScheduleWeekDays + "', '" + strCurrentDate + "','" + strCurrentDate + "',0,0,0,null,'" + strCurrentDate + "',0,'" + strCurrentDate + "','" + strCurrentDate + "', '" + attachments + "',0,0,0,-1," + contactCount + ",-1,null,-1,null,0,2,2)";
        string insertRecordStr = "Insert into PortalCommModule_ScheduledEvents Values " + insertQuery + ";SELECT SCOPE_IDENTITY()";
        cmd = new SqlCommand(insertRecordStr, con);
        con.Open();
        var val = cmd.ExecuteScalar().ToString();
        con.Close();

        scheduleItem.TemplateId = templateId;
        scheduleItem.AddToCommLog = false;
        scheduleItem.ClientId = clientIds;
        scheduleItem.NoticeType = NoticeType;
        scheduleItem.ScheduleEndDate = onceDeliveryDate;
        scheduleItem.ScheduleStartDate = onceDeliveryDate;
        scheduleItem.NextDeliveryDate = onceDeliveryDate;
        scheduleItem.ScheduleToList = emailIds;
        scheduleItem.ScheduleType = scheduleType;
        scheduleItem.DeliveryType = deliveryType;
        scheduleItem.IsActive = true;
        scheduleItem.IsScheduled = false;
        scheduleItem.ScheduleCreatedBy = null;
        scheduleItem.ScheduleCreatedDate = DateTime.UtcNow.Date;
        scheduleItem.HoldFollowUps = false;
        scheduleItem.ScheduleWeekDays = "";
        scheduleItem.DeliveredDate = onceDeliveryDate;
        scheduleItem.Attachments = "";
        scheduleItem.ContactIds = contactIds;
        scheduleItem.EndByOccurances = 0;
        scheduleItem.WeekOccurances = 0;
        scheduleItem.TemplateBody = htmlContent;
        scheduleItem.OccurancesCompleted = 0;
        scheduleItem.AttachmentId = -1;
        scheduleItem.SelectedContactCount = contactCount;
        scheduleItem.ScheduleCcList = false;
        scheduleItem.AddToCommLog = false;
        scheduleItem.MonthlyType = -1;
        scheduleItem.MonthlyValue = null;
        scheduleItem.YearlyType = -1;
        scheduleItem.YearlyValue = null;
        scheduleItem.ScheduleId = Convert.ToInt32(val);       
        return scheduleItem;
      }
      catch(Exception ex)
      {
        Console.Out.WriteLine(ex.Message);
        logger.Error(ex.Message);
        con.Close();
        return scheduleItem;
      }
    }

    static void AddEmailRecord(List<ClientIdEmailModel> clientEmailList, PortalCommModule_ScheduledEvents scheduleEvent, SqlConnection con, string emailFromUrl, string companyName, string templateSubject)
    {
      try
      {
        List<string> insertQuery = new List<string>();
        string insertRecordStr = string.Empty;
        string includeCC = "";
        foreach (ClientIdEmailModel clientObj in clientEmailList)
        {
          if (clientObj.Type == "Contact")
          {
            insertQuery.Add("('" + emailFromUrl + "','" + clientObj.Email + "','" + includeCC + "','" + templateSubject.Replace("'", "''") + "','" + clientObj.emailContent.Replace("'", "''") + "'," + clientObj.ClientId + "," + clientObj.ContactAdvisorId + ",0,GETUTCDATE(),'" + scheduleEvent.ScheduleCreatedBy + "'," + scheduleEvent.AttachmentId + ",0,-1,GETUTCDATE(),0," + scheduleEvent.ScheduleId + ",1,2,null,null,null)");
          }
          else
          {
            insertQuery.Add("('" + emailFromUrl + "','" + clientObj.Email + "','" + includeCC + "','" + templateSubject.Replace("'", "''") + "', '" + clientObj.emailContent.Replace("'", "''") + "'," + clientObj.ClientId + ",0," + clientObj.ContactAdvisorId + ",GETUTCDATE(),'" + scheduleEvent.ScheduleCreatedBy + "'," + scheduleEvent.AttachmentId + ",0,-1,GETUTCDATE(),0," + scheduleEvent.ScheduleId + ",1,2,null,null,null)");
          }
        }
        var commaQuery = string.Join(",", insertQuery.ToArray());
        insertRecordStr = "Insert into EmailRecord Values " + commaQuery;
        ExecuteSqlCommandUpdate(insertRecordStr, con);
      }
      catch(Exception ex)
      {
        Console.Out.WriteLine(ex.Message);
      }
    }

  }
  public class ClientIdEmailModel
  {
    public int ClientId { get; set; }
    public string Email { get; set; }
    public string Type { get; set; }
    public int ContactAdvisorId { get; set; }
    public string ContactAdvisorName { get; set; }
    public string emailContent { get; set; }
    public string includeCC { get; set; }
  }
  public class PortalCommModuleHoliday
  {
    public int Id { get; set; }
    public string HolidayName { get; set; }
    public int HolidayDays { get; set; }
    public DateTime HolidayDate { get; set; }
    public string HolidayDateText { get; set; }
    public DateTime DeliveryDate { get; set; }
    public string DeliveryDateText { get; set; }
    public string ImageName { get; set; }
    public string ImagePath { get; set; }
    public int TemplateId { get; set; }
    public string TemplateName { get; set; }
    public int DaysBefore { get; set; }
    public bool IsFederalHoliday { get; set; }
    public bool IsObserved { get; set; }
    public bool SendToContacts { get; set; }
    public bool SendToAdvisors { get; set; }
    public DateTime HolidayStartDate { get; set; }
    public string HolidayStartDateText { get; set; }
  }

  public class PortalDefaultsBlobDetails
  {
    public int Id { get; set; }
    public string BlobContainerName { get; set; }
    public string BlobConnectionKey { get; set; }
    public string BlobConnectionString { get; set; }
  }

  public class PortalCommModuleTemplates
  {
    public int Id { get; set; }
    public string AzureTemplateId { get; set; }
    public string TemplateName { get; set; }
    public string TemplateSubject { get; set; }
    public string HtmlPath { get; set; }
    public int NoticeType { get; set; }
    public int CreatedBy { get; set; }
    public string CreatedDate { get; set; }
    public string TemplatePreHeader { get; set; }
    public int DeliveryType { get; set; }
    public string LastUpdatedDate { get; set; }
    public int LastUpdatedBy { get; set; }
    public bool IsDefault { get; set; }
  }

  public class CommModuleContactAdvisor
  {
    public int id { get; set; }
    public string label { get; set; }
    public int ClientID { get; set; }
    public string Email { get; set; }
    public bool ticked { get; set; }
    public string ClientName { get; set; }
    public string Type { get; set; }
  }
  public class CommModule_Predefined
  {
    public int Id { get; set; }
    public int TemplateId { get; set; }
    public string PredefinedKey { get; set; }
    public string PredefinedValue { get; set; }
  }
  public partial class PortalCommModule_ScheduledEvents
  {
   
    public int ScheduleId { get; set; }
    public int TemplateId { get; set; }
    public string TemplateBody { get; set; }
    public int NoticeType { get; set; }
    public int ScheduleType { get; set; }
    public int DeliveryType { get; set; }
    public string ClientId { get; set; }
    public string ContactIds { get; set; }
    public string ScheduleToList { get; set; }
    public Nullable<bool> ScheduleCcList { get; set; }
    public string ScheduleWeekDays { get; set; }
    public Nullable<System.DateTime> ScheduleStartDate { get; set; }
    public Nullable<System.DateTime> ScheduleEndDate { get; set; }
    public Nullable<bool> IsActive { get; set; }
    public Nullable<bool> AddToCommLog { get; set; }
    public Nullable<bool> HoldFollowUps { get; set; }
    public Nullable<int> ScheduleCreatedBy { get; set; }
    public Nullable<System.DateTime> ScheduleCreatedDate { get; set; }
    public bool IsScheduled { get; set; }
    public Nullable<System.DateTime> DeliveredDate { get; set; }
    public Nullable<System.DateTime> NextDeliveryDate { get; set; }
    public string Attachments { get; set; }
    public Nullable<int> EndByOccurances { get; set; }
    public Nullable<int> OccurancesCompleted { get; set; }
    public Nullable<int> WeekOccurances { get; set; }
    public int AttachmentId { get; set; }
    public int SelectedContactCount { get; set; }
    public Nullable<int> MonthlyType { get; set; }
    public string MonthlyValue { get; set; }
    public Nullable<int> YearlyType { get; set; }
    public string YearlyValue { get; set; }
    public bool AddToEmailHistory { get; set; }

   
  }
}
